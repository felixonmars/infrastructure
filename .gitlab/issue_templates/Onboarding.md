<!--
This template should be used for onboarding new Arch Linux team members.
It can also be used as a reference for adding new roles to an existing team member.
-->

# Onboarding an Arch Linux team member

## Details

- **Team member username**:
- **Application**: <!-- Add link to relevant mailing list mail -->
  - **Voting result**: <!-- Add link to relevant mailing list mail -->

## All roles checklist
The mailing list password can be found in misc/additional-credentials.vault.

- [ ] Add new user email as per `docs/email.md`.
- [ ] Create a new user in archweb: https://www.archlinux.org/devel/newuser/
      This is also linked in the django admin backend at the top
- [ ] Subscribe user to internal [staff mailing list](https://lists.archlinux.org/admin/staff/members/add)
- [ ] Give the user access to `#archlinux-staff` on Libera Chat
- [ ] Give the user a link to our [staff services page](https://wiki.archlinux.org/title/DeveloperWiki:Staff_Services)

## Packager onboarding checklist

<!-- The ticket should be created by a sponsor of the new packager -->
- [ ] Create [issue in archlinux-keyring](https://gitlab.archlinux.org/archlinux/archlinux-keyring/-/issues/new) (choose *"New Packager Key"* template)

## Main key onboarding checklist

- [ ] Add new user email for the `master-key.archlinux.org` subdomain as per `docs/email.md`.
<!-- The ticket should be created by the developer becoming a new main key holder -->
- [ ] Create [issue in archlinux-keyring](https://gitlab.archlinux.org/archlinux/archlinux-keyring/-/issues/new) (choose *"New Main Key"* template)

## Developer onboarding checklist

- [ ] Add entry in `group_vars/all/archusers.yml`.
- [ ] Add SSH pubkey to `pubkeys/<username>.pub`.
- [ ] Run `ansible-playbook -t archusers playbooks/*.yml`.
- [ ] Assign the user to the `Developers` groups on Keycloak.
- [ ] Assign the user to the `Developers` group on [archlinux.org](https://archlinux.org/admin/auth/user/)
- [ ] Subscribe user to internal [arch-dev mailing list](https://lists.archlinux.org/admin/arch-dev/members/add)
- [ ] Whitelist email address on [arch-dev-public](https://lists.archlinux.org/admin/arch-dev-public/members) (find member and unmoderate)

## TU onboarding checklist

- [ ] Add entry in `group_vars/all/archusers.yml`.
- [ ] Add SSH pubkey to `pubkeys/<username>.pub`.
- [ ] Run `ansible-playbook -t archusers playbooks/*.yml`.
- [ ] Assign the user to the `Trusted Users` groups on Keycloak.
- [ ] Assign the user to the `Trusted Users` group on [archlinux.org](https://archlinux.org/admin/auth/user/)
- [ ] Whitelist email address on [arch-dev-public](https://lists.archlinux.org/admin/arch-dev-public/members) (find member and unmoderate)
- [ ] Subscribe user to internal [arch-tu mailing list](https://lists.archlinux.org/admin/arch-tu/members/add)

## DevOps onboarding checklist

- [ ] Add entries in `group_vars/all/root_access.yml`.
- [ ] Run `ansible-playbook -t root_ssh playbooks/all-hosts-basic.yml`.
- [ ] Run `ansible-playbook playbooks/hetzner_storagebox.yml playbooks/rsync.net.yml`.
- [ ] Assign the user to the `DevOps` group on Keycloak.
- [ ] Subscribe user to [arch-devops-private mailing lists](https://lists.archlinux.org/admin/arch-devops-private/members/add)
- [ ] Add pubkey to [Hetzner's key management](https://robot.your-server.de/key/index) for Dedicated server rescue system.

## Wiki Administrator checklist

- [ ] Assign the user to the `Wiki Admins` group on Keycloak.
- [ ] Subscribe the user to the [arch-wiki-admins mailing list](https://lists.archlinux.org/admin/arch-wiki-admins/members/add).
